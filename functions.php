<?php
/**
 * Child Starter functions and definitions
 *
 */
use Timber\Timber;

add_action( 'wp_enqueue_scripts', function() {
    wp_enqueue_style('theme-override', get_stylesheet_directory_uri() . '/static/base.min.css', array(), '1.0.0', 'all');
});

add_action('wp_head', function() {
	Timber::render( 'core/favicons.twig');
});

add_action('wp_head', function() {
	$user = wp_get_current_user();
	if($user->exists()) {
		if(str_contains($user->user_email, '@imsmarketing.ie')) {
			?>
			<link rel="preconnect" href="https://fonts.googleapis.com">
			<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
			<link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@300;400;500;600;700&display=swap" rel="stylesheet">
			<script>
			  window.markerConfig = {
				project: '6405f9cc59d6130b1b477f37', 
				source: 'snippet',
				reporter: {
					email: 'vini@imsmarketing.ie',
					fullName: 'IMS Marketing',
				},
			  };
			</script>
			<?php
		} else {
			?>
			<script>
			  window.markerConfig = {
				project: '6405f9cc59d6130b1b477f37', 
				source: 'snippet',
				reporter: {
					email: '<?= $user->user_email; ?>',
					fullName: '<?= $user->display_name; ?>',
				},
			  };
			</script>
			<?php
		}
		?>
		<script>
			!function(e,r,a){if(!e.__Marker){e.__Marker={};var t=[],n={__cs:t};["show","hide","isVisible","capture","cancelCapture","unload","reload","isExtensionInstalled","setReporter","setCustomData","on","off"].forEach(function(e){n[e]=function(){var r=Array.prototype.slice.call(arguments);r.unshift(e),t.push(r)}}),e.Marker=n;var s=r.createElement("script");s.async=1,s.src="https://edge.marker.io/latest/shim.js";var i=r.getElementsByTagName("script")[0];i.parentNode.insertBefore(s,i)}}(window,document);
		</script>
		<?php
	}
  });