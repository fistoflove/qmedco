<?php

use Timber\Timber;
use Timber\Post;
use IMSWP\Helper\Scss;

use IMSWP\Helper\Helper;

include_once("fields.php");

$context = Timber::context();

$context['post'] = new Post(get_the_ID());

$context['block'] = $block;

$context['fields'] = get_fields();

$context['is_preview'] = $is_preview;

// Helper::imageFromObject([ 'image' => $context['fields']['content']['image']]);

Timber::render( 'template.twig', $context);