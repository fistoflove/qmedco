<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Form",
    [
        ["ID", "text"],
    ]
);

$fields->register_tab(
    "Overlay",
    [
        ["Phone", "wysiwyg"],
        ["Email", "wysiwyg"],
        ["Address", "wysiwyg"],
    ]
);